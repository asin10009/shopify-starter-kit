'use strict';

var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    //bourbon = require('node-bourbon'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    wait = require('gulp-wait2');
var browserSync = require('browser-sync').create();
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
// var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');


// Scss stylesheets
gulp.task('stylesheets', function() {
    return gulp.src('styles/**/*.scss')
        .pipe(wait(150))
        .pipe(sass({
            outputStyle: 'compressed',
        })).on('error', sass.logError)
        .pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
        .pipe(gulp.dest('theme/assets/'))
});

gulp.task('watch', function() {
    watch(['styles/scss/**/*.scss'], function(event, cb) {
        gulp.start('stylesheets');
    });
    watch(['scripts/**/*.js'], function(event, cb) {
        gulp.start('js');
    });
    // watch()

});

gulp.task('js', function() {
    // app.js is your main JS file with all your module inclusions
    return browserify({
            entries: 'scripts/main.js',
            debug: true
        })
        .transform("babelify", {
            presets: ["es2015", "es2016"]
        })
        .bundle()
        .pipe(source('app.js'))
        .pipe(buffer())
        // .pipe(sourcemaps.init())
        // .pipe(uglify())
        // .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('theme/assets/'))
});

// Run
gulp.task('default', [
    'stylesheets',
    'js',
    // 'copy',
]);